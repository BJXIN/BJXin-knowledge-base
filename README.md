# **关于本站**

<p align="center">
    <a href="https://www.bjxdglss.top/" target="_blank">
        <img src="https://gitee.com/bjxin/JXin-TC/raw/master/knowledge/banner.png" width=""/>
    </a>
</p>

> [!NOTE]
> 
>  本站为自学者提供免费视频课程，课程来源于网络，保存在个人网盘上，如有侵权，请联系站长删除！
>  
>  旨在为自学者提供一系列：
>  - **靠谱的资源**
>  - **清晰的视频教程**
> 
> 方便自己也方便他人。网站内容会保持**持续更新**，欢迎收藏品鉴！

<h2 align="center">联系站长</h2>
<p align="center">
  <a href="https://gitee.com/bjxin" target="_blank"><img src="https://gitee.com/bjxin/JXin-TC/raw/master/Icon/Gitee.png" width="45" height="45"></a>
  <a href="https://space.bilibili.com/170963474" target="_blank"><img src="https://gitee.com/bjxin/JXin-TC/raw/master/Icon/Bilibili.png" width="50" height="50"></a>
  <a href="https://mp.weixin.qq.com/s/mj7deOoBp4IKJ_-iD9-E6g" target="_blank">
    <img src="https://gitee.com/bjxin/JXin-TC/raw/master/Icon/Wechat.png" width="50" height="50"></a>
  <a href="https://github.com/BJXIN" target="_blank"><img src="https://gitee.com/bjxin/JXin-TC/raw/master/Icon/Github.png" width="50" height="50"></a>
</p>


---


# **视频课程分类**
> [!NOTE]
>  本站目前整理收录了30大类的视频课程，具体如下：
>  
>  <img src="https://gitee.com/bjxin/JXin-TC/raw/master/knowledge/zyfl.png">
> 
> 后续本站也会持续更新和增加更多类型的视频课程......

---

<!-- tabs:start -->

### **语言**
> 提取码
~~~
51ep
~~~

#### **粤语**
- [**从粤语小白晋升为粤语达人**](https://www.aliyundrive.com/s/EQWMcX9mkDr)

#### **日语**
- [**唐盾：0-N4日语精讲**](https://www.aliyundrive.com/s/QQHffABohJQ)
- [**20节课巧用中文学日语**](https://www.aliyundrive.com/s/iQZJSzevQiM)

#### **英语**
- [**20小时掌握英语核心秘诀**](https://www.aliyundrive.com/s/XMF69emK8G6)
- [**21天暴力突破英语视频课程**](https://www.aliyundrive.com/s/BxCoGD7bmm8)
- [**KO大魔王：英语语法通关计划**](https://www.aliyundrive.com/s/FsvQbd7sd3t)
- [**外教口语课让你英语脱口而出**](https://www.aliyundrive.com/s/FbS1Uo9frQJ)
- [**孙志立简明音标50讲**](https://www.aliyundrive.com/s/B5vmykN6iN9)
- [**孙志立英语自然拼读100讲**](https://www.aliyundrive.com/s/eD295DoGPLj)
- [**母语思维30小时吃透英语语法**](https://www.aliyundrive.com/s/sG1ohybH2US)
- [**每天8分钟小白英语入门课**](https://www.aliyundrive.com/s/d9B3xFo72Ts)
- [**精品课堂：英语四级全程班**](https://www.aliyundrive.com/s/pykb2M3huaY)
- [**虾蟹美音零基础发音班视频课**](https://www.aliyundrive.com/s/1tsSG9cVb2E)
- [**颠覆你的传统英语学习课程**](https://www.aliyundrive.com/s/ha4YPPMfU38)
- [**马思瑞的口语私教视频课程**](https://www.aliyundrive.com/s/AV6izHqPx2H)

### **声音**
> 提取码
~~~
sy79
~~~
- [**胖雪人的声音魔法课**](https://www.aliyundrive.com/s/uNCV5twrQbB)
- [**李蕾声优课**](https://www.aliyundrive.com/s/SsvMtH9KvsH)
- [**声音赚钱的秘密 有声书进阶课**](https://www.aliyundrive.com/s/795cPJYrgW7)

### **音乐**
> 提取码
~~~
yyjc
~~~
- [**说唱分享课程**](https://www.aliyundrive.com/s/DKm79VEzDFL)
- [**方锦龙国乐通识课**](https://www.aliyundrive.com/s/i7aQBwKtM7g)

#### **演唱**
- [**从零基础到歌手混声训练营**](https://www.aliyundrive.com/s/jLABJvNAau9)
- [**杨草飞唱歌-细节教学视频**](https://www.aliyundrive.com/s/kus5NoDpsQt)
- [**王力宏线上唱歌教程**](https://www.aliyundrive.com/s/fUk6ScnQv4R)
- [**轻松学习流行演唱**](https://www.aliyundrive.com/s/ofAYo9bNUti)

#### **乐理**
- [**【音律屋】乐理课程**](https://www.aliyundrive.com/s/ygryhstBdEa)
- [**从零开始学乐理**](https://www.aliyundrive.com/s/66LUBNJjCNJ)

#### **创作**
- [**全能音乐人入门训练营:零基础学写歌**](https://www.aliyundrive.com/s/wd7yFD1Tu9i)
- [**方文山月学作词课程**](https://www.aliyundrive.com/s/Uw32vb314Uk)
- [**midi创作班**](https://www.aliyundrive.com/s/fiu7xJL6DAr)
- [**小贝编曲课**](https://www.aliyundrive.com/s/1hq2MQRzcWM)

#### **制作**
- [**独立音乐制作人微专业**](https://www.aliyundrive.com/s/74e12csvVdn)
- [**Mai 说唱音乐制作系列**](https://www.aliyundrive.com/s/Z1NPn7XnQig)

#### **音乐制作宿主软件教程**
- [**Adobe Audition CS6**](https://www.aliyundrive.com/s/2tPkKme3DP4)
- [**手把手教玩转库乐队**](https://www.aliyundrive.com/s/q4pnQju7Dib)
##### **FL Studio 20**
- [**FL Studio 20 快速上手**](https://www.aliyundrive.com/s/UQRV6ZTHUNn)
- [**FL studio 20 水果中文操作入门教程**](https://www.aliyundrive.com/s/BVLppEjYK8z)

#### **混音**
- [**王三石混音课**](https://www.aliyundrive.com/s/uALMHGoKpeF)
- [**周天澈原版混音课**](https://www.aliyundrive.com/s/7jB1DejWbPo)

#### **乐器**

##### **吉他**
- [**谭秋娟的吉他弹唱入门课**](https://www.aliyundrive.com/s/JGJkegx6Kh3)

##### **尤克里里**
- [**AI音乐学院尤克里里**](https://www.aliyundrive.com/s/stMQG63BHqL)
- [**尤克里里指弹基本功**](https://www.aliyundrive.com/s/QDoBKVUPDxc)

##### **钢琴**
- [**钢琴考级教程1-10级**](https://www.aliyundrive.com/s/w3UUwcKBibz)
- [**郎朗手把手带你开启钢琴之路**](https://www.aliyundrive.com/s/AvoXQrEGNXd)
- [**小贝零基础钢琴教学**](https://www.aliyundrive.com/s/FELY5xkwfNm)
- [**小贝教你弹即兴**](https://www.aliyundrive.com/s/zMGdE8vjVbx)
- [**从零开始学钢琴：圆你儿时钢琴梦**](https://www.aliyundrive.com/s/1AyKiyUx2XS)

### **艺术**
> 提取码
~~~
97ys
~~~
- [**零基础也能快速变身抖音网红舞达人**](https://www.aliyundrive.com/s/H3evAH548Un)
- [**时代音画**](https://www.aliyundrive.com/s/XjGTxKtkZR9)

### **绘画**
> 提取码
~~~
huih
~~~
- [**李舜教你画卡通IP视频课程**](https://www.aliyundrive.com/s/AqxEeHKxV41)
- [**原画人物头像基础视频教程**](https://www.aliyundrive.com/s/geZbE1Vu4DJ)
- [**写实人像线条造型训练法视频教程**](https://www.aliyundrive.com/s/NT35jXyWciP)

#### **插画**
- [**超简单插画课：一个月解锁手绘技能**](https://www.aliyundrive.com/s/GsXZKmfrFt2)
- [**快速学会十二星座之插画**](https://www.aliyundrive.com/s/F84Qkna5Sai)
- [**十分绘画：绘画萌新入门创造营**](https://www.aliyundrive.com/s/QXyTtAxC81k)

#### **手绘**
- [**随学随用的零基础手绘课**](https://www.aliyundrive.com/s/5o74MrdgQH4)
- [**手绘表达：用视觉思维练就审美**](https://www.aliyundrive.com/s/e96vPpLc68i)
- [**幻想类古风手绘视频课程**](https://www.aliyundrive.com/s/rTC9P24PGPX)

### **动画**
> 提取码
~~~
dh34
~~~
- [**大神同款定格动画保姆级教学**](https://www.aliyundrive.com/s/BxphxuKEcGm)

### **制图**
> 提取码
~~~
98zt
~~~
- [**创意合成修图课程**](https://www.aliyundrive.com/s/BBcCNQ4LhbV)
- [**玄幻风广告图后期创意合成**](https://www.aliyundrive.com/s/5z9Xc9AwbvM)

#### **Photoshop**
- [**Photoshop从小白到高手**](https://www.aliyundrive.com/s/WcwuuWcBrNW)
- [**PS异闻录：萌新系统入门课**](https://www.aliyundrive.com/s/mf8LAv4GstF)

### **学术**
> 提取码
~~~
xs97
~~~
- [**简易命理学：教你学会看八字**](https://www.aliyundrive.com/s/g1e1nX9B4z1)
- [**领略环球博物馆中的自然科学**](https://www.aliyundrive.com/s/Cj4VLBixtsh)

#### **戴建业老师**
- [**戴建业：精讲世说新语**](https://www.aliyundrive.com/s/CJWjawxF9kq)
- [**戴建业高能诗词课**](https://www.aliyundrive.com/s/5wwM8vtypdH)

#### **数学**

- [**7天学会数学建模及MATLAB编程**](https://www.aliyundrive.com/s/oirvehn1zY9)
- [**摆脱题海奥数班，24堂颠覆传统的数学课高效提升成绩**](https://www.aliyundrive.com/s/5VnMK8oJ4HW)

##### **微积分**
- [**清华大学入门微积分视频课**](https://www.aliyundrive.com/s/Lh3iLmFVfXY)
- [**19课时学完微积分全集课程**](https://www.aliyundrive.com/s/qwLkoDG8D5n)

#### **论文**
- [**30天论文写作发表进阶教程**](https://www.aliyundrive.com/s/xAuMHT1FBfc)
- [**Word论文排版精讲**](https://www.aliyundrive.com/s/JmNKfB6SqFc)

##### **SCI**
- [**零基础，零代码发表4分生信SCI**](https://www.aliyundrive.com/s/1t2UjPQWm97)
- [**SCI期刊论文写作**](https://www.aliyundrive.com/s/i9MiHcvB2S9)

### **记忆**
> 提取码
~~~
jiyi
~~~
- [**轻松提高记忆力**](https://www.aliyundrive.com/s/7yxRpUvqbxe)
- [**20堂超实用记忆术，教你快速记住任何信息!**](https://www.aliyundrive.com/s/SXGuN7gWqw4)
- [**申一帆 人人都可以学会的超级记忆法**](https://www.aliyundrive.com/s/7Ez2Zy1STX4)

#### **菲常记忆**
- [**九节课练就你的超强大脑**](https://www.aliyundrive.com/s/7BzSRQhiE6U)
- [**卢菲菲记忆大师教你提升记忆力**](https://www.aliyundrive.com/s/9noxS6cQNJ5)
- [**菲常记忆超脑特训营视频课程**](https://www.aliyundrive.com/s/8kk2wt2VXhb)

### **写作**
> 提取码
~~~
xz99
~~~
- [**14堂零基础小说写作课，从新手到网络畅销作家**](https://www.aliyundrive.com/s/J3CHUbcAEkC)
- [**28天一站式解决你所有的写作痛点**](https://www.aliyundrive.com/s/Juf1Z9SJhph)
- [**新媒体爆款写作指南**](https://www.aliyundrive.com/s/ySVAQGTxk9b)
- [**如何快速写出10W+爆款文章**](https://www.aliyundrive.com/s/XRBY7k1JNgs)
- [**树獭先生 如何利用空余时间写作，实现月入十万**](https://www.aliyundrive.com/s/inqz2uhKxvw)
- [**欢喜印象笔记 写作系统行动营**](https://www.aliyundrive.com/s/diCaazJpkqw)
- [**猫博士的超级作文课**](https://www.aliyundrive.com/s/QkyQEZdG9we)
- [**零基础写作赚钱，业余时间也能月入过万**](https://www.aliyundrive.com/s/Y4Q9KXeGzK3)

#### **文案**
- [**12节文案课**](https://www.aliyundrive.com/s/2JpA1kfcZbu)
- [**七节课教你写出杀手级文案**](https://www.aliyundrive.com/s/jK7FHRyrCKe)
- [**快速掌握10年职业文案人绝活**](https://www.aliyundrive.com/s/gspoN85VNvm)

### **文字**
> 提取码
~~~
99wz
~~~
- [**每天15分钟，21天带你写出一手漂亮好字**](https://www.aliyundrive.com/s/bwAA8ZLZkZL)
- [**每天15分钟3周写出一手漂亮字**](https://www.aliyundrive.com/s/B5r7U25cJke)

### **设计**
> 提取码
~~~
z54m
~~~
- [**服装色彩搭配**](https://www.aliyundrive.com/s/Q6N5hhJdez4)
- [**海报色彩实验室训练营**](https://www.aliyundrive.com/s/bb1yPRQK9w1)
- [**全领域LOGO设计特训营**](https://www.aliyundrive.com/s/3PfC1etbG15)
- [**怪兽工厂：游戏场景设计篇**](https://www.aliyundrive.com/s/uCS2MfZidjg)
- [**像企鹅一样做设计产品课**](https://www.aliyundrive.com/s/kzdxsPK9Hm4)

#### **字体设计**
- [**治字百方—左佐字体设计课**](https://www.aliyundrive.com/s/g8kYvPJ6nPW)
- [**字体设计从生活创意到逻辑实践**](https://www.aliyundrive.com/s/5GyPfsD5yC1)
- [**字体从基本功到创意风格课程**](https://www.aliyundrive.com/s/cBuS1VGNUSG)

#### **UI设计**
- [**商业UI全链式设计方法与流程**](https://www.aliyundrive.com/s/dWBLo7qSChS)
- [**突破设计瓶颈：做好UI设计师**](https://www.aliyundrive.com/s/gTX6PDctPjf)
- [**UI设计领域实战视频教程**](https://www.aliyundrive.com/s/DmQp8RD6SLA)

#### **电商设计**
- [**电商海报设计基础视频教程**](https://www.aliyundrive.com/s/owxqyPnDfLS)

##### **侯帅**
- [**侯帅电商主图直通车图设计**](https://www.aliyundrive.com/s/5MHGSogiVcf)
- [**好看能带货的电商详情页设计**](https://www.aliyundrive.com/s/abjrDWdpxJg)

### **生活**
> 提取码
~~~
sh74
~~~
- [**快速解决失眠 教你学会自我催眠术**](https://www.aliyundrive.com/s/5JngFiEedXZ)
- [**买车攻略**](https://www.aliyundrive.com/s/WYyFnFGwK58)
- [**中国手语培训**](https://www.aliyundrive.com/s/9F3jeNmHSib)

#### **房屋**
- [**500元懒人也能营造温馨有格调的小窝**](https://www.aliyundrive.com/s/PagbrZKynZY)
- [**超100种收纳方法，整理出一个舒心惬意的家**](https://www.aliyundrive.com/s/3f6g7f1trF4)

##### **买房**
- [**买房干货笔记**](https://www.aliyundrive.com/s/ab1tQLb2mqo)
- [**月薪五千实现的买房全攻略，教你买对人生的第一套房**](https://www.aliyundrive.com/s/5DhReo4PFZQ)

### **餐饮**
> 提取码
~~~
co14
~~~

- [**许岑和秦延庆的炒菜教程**](https://www.aliyundrive.com/s/1cN5YCPkTnK)
- [**莽哥餐饮实体店引流课程**](https://www.aliyundrive.com/s/fpi1qoEYnDZ)
- [**花式咖啡速成咖啡拉花技巧**](https://www.aliyundrive.com/s/gZmua95g6Rm)
- [**烤鱼全流程案例视频课程**](https://www.aliyundrive.com/s/iMb2fnEZqZC)

### **财务**
> 提取码
~~~
29cw
~~~
- [**0基础学会财务分析和经营决策**](https://www.aliyundrive.com/s/4aucJE2tXcs)
- [**可转债投资课**](https://www.aliyundrive.com/s/PQqPd3kC1z3)

#### **理财**
- [**简七32堂理财课**](https://www.aliyundrive.com/s/93Zi6GW4PUa)
- [**理财赚钱：50个低风险理财大全**](https://www.aliyundrive.com/s/G3Eg24pjVPR)


### **社交**
> 提取码
~~~
sjqg
~~~
- [**追求的艺术**](https://www.aliyundrive.com/s/uxxis1q5NPR)
- [**情绪罗盘：探测你的喜怒哀乐**](https://www.aliyundrive.com/s/oydtsHs1ZTq)
- [**樊登：可复制的沟通力课程**](https://www.aliyundrive.com/s/C3poLn2Hp8r)
- [**八节课帮你攻克社交障碍课程**](https://www.aliyundrive.com/s/cmLeyWjbHtJ)
- [**20个公式学会幽默之道**](https://www.aliyundrive.com/s/c7TJxQh6ubs)
- [**让你通过聊天方法炼成强大心态**](https://www.aliyundrive.com/s/jwhTLsKNp2P)

#### **EQ**
- [**提高情商，改变人生的读心术**](https://www.aliyundrive.com/s/LKnJyXy9rhu)
- [**15堂实用有效的高情商沟通课**](https://www.aliyundrive.com/s/DG3JHFE649K)
- [**高情商话术**](https://www.aliyundrive.com/s/82kyYFyqD2D)

#### **表达**
- [**即学即用的12堂演讲高效表达法**](https://www.aliyundrive.com/s/wXiV9FZNpNA)
- [**学会公共表达**](https://www.aliyundrive.com/s/s9EGUMHKGph)

### **法律**
> 提取码
~~~
falv
~~~
- [**罗翔：刑法悖论十讲**](https://www.aliyundrive.com/s/7DiTwrKY3yb)
- [**法律尽职调查实务系统班**](https://www.aliyundrive.com/s/F34A2zCK9a2)

### **心理**
> 提取码
~~~
95xl
~~~
- [**社会心理学认知与理解**](https://www.aliyundrive.com/s/xSBoLdsTC8Q)
- [**用心理学改变自己的7大习惯**](https://www.aliyundrive.com/s/1zvA55mWwcN)
- [**巧用心理学过更有效率的人生**](https://www.aliyundrive.com/s/PFbckkDszCh)
- [**叙事心理疗法进阶视频课程**](https://www.aliyundrive.com/s/eCmk1fUZtMv)

### **摄影**
> 提取码
~~~
shey
~~~
- [**FOTOLAB摄影布光课程**](https://www.aliyundrive.com/s/eFRXNwHFZk1)
- [**婚礼拍摄思路全流程**](https://www.aliyundrive.com/s/im5tj6p2hXt)

#### **美食摄影**
- [**新派美食商业摄影思路与技巧**](https://www.aliyundrive.com/s/opkSaSs3QGE)
- [**美食摄影师从小白到视觉盛宴**](https://www.aliyundrive.com/s/Dfk7D7s2wjP)

#### **手机摄影**
- [**用手机拍摄专业级视频短片**](https://www.aliyundrive.com/s/UPoxfrrznJu)
- [**何雄手机摄影教程**](https://www.aliyundrive.com/s/JibVhA8zQ2v)

### **剪辑**
> 提取码
~~~
jj55
~~~
- [**零基础系统学剪辑思维训练营**](https://www.aliyundrive.com/s/tmEwezJmYyn)
- [**超实用的高效视频调色方法课程**](https://www.aliyundrive.com/s/GR8g4ttKGcQ)
- [**短视频学院拍视频摄剪辑核心课**](https://www.aliyundrive.com/s/mEQSNTZZjX1)
- [**小白易上手的手机剪辑**](https://www.aliyundrive.com/s/qALaypUcNqn)

### **影视**
> 提取码
~~~
9y7s
~~~
- [**电影自习室**](https://www.aliyundrive.com/s/Gnv7KVzAQeq)
- [**电影剧本写作技巧提升分析**](https://www.aliyundrive.com/s/zMh89ycnHtx)
- [**手机拍摄短片导演**](https://www.aliyundrive.com/s/a7QbnBoHSNM)
- [**影视文案策划快速提升课**](https://www.aliyundrive.com/s/um85etF1iz6)
- [**分镜头脚本创作**](https://www.aliyundrive.com/s/EYzuG8PAGNJ)
- [**从零开始掌握视频导演全部技能**](https://www.aliyundrive.com/s/38cT4Jpgotr)
- [**从创作开始踏入电影界视频课程**](https://www.aliyundrive.com/s/PbrfYmXHsqu)
- [**「CSC电影学院」李伟-电影短片创作**](https://www.aliyundrive.com/s/zZJEjwmcbFe)
- [**6节课带你跨入职业编剧大门**](https://www.aliyundrive.com/s/gR24FbKiY6i)
- [**12节影评变现课**](https://www.aliyundrive.com/s/28o1aUhWqJh)

### **娱乐**
> 提取码
~~~
yvle
~~~
- [**从0开始学习你的第一堂剧本杀**](https://www.aliyundrive.com/s/GjFhVu47cnk)
- [**元宇宙NFT行业入门必修课**](https://www.aliyundrive.com/s/aXiztjVm5Sp)

### **科技**
> 提取码
~~~
keji
~~~
- [**Mac电脑入门指南视频教程**](https://www.aliyundrive.com/s/WHHYsR23yHy)
- [**Linux从零入门实战：边学边练**](https://www.aliyundrive.com/s/RJL8DUtsyYN)
- [**Web安全：白帽子黑客训练营**](https://www.aliyundrive.com/s/zDmDRWxmU37)
- [**京东商业化数据分析师培养计划**](https://www.aliyundrive.com/s/XYbHM3GuPuz)
- [**人工智能深度学习入门课程**](https://www.aliyundrive.com/s/icxLGPZ4K3c)
- [**信息系统项目管理师高项课程**](https://www.aliyundrive.com/s/FvoR4Cja8HP)
- [**无人机智能高级实战视频课程**](https://www.aliyundrive.com/s/NvnTsBEXCgm)
- [**麦子学院 网络安全入门教程**](https://www.aliyundrive.com/s/nQhdJKSceR9)

### **编程**
> 提取码
~~~
qq50
~~~

#### **易语言**
- [**觅风全套易语言教程**](https://www.aliyundrive.com/s/eUB9L4uy53D)

#### **小程序**
- [**5小时零基础入门小程序云开发**](https://www.aliyundrive.com/s/aZJ4mc4QRdR)
- [**小程序开发从入门到精通视频课**](https://www.aliyundrive.com/s/Aj6N6aQMk9E)

#### **网站**
- [**H5**](https://www.aliyundrive.com/s/EfCMUKe61o6)
- [**Web前端入门：从零开始做网站**](https://www.aliyundrive.com/s/2YH8zGzTX4a)
- [**joomla3快速建站全纪录视频课**](https://www.aliyundrive.com/s/c7ieEzpJ4ft)

#### **Python**
- [**三小时变身Python极客**](https://www.aliyundrive.com/s/kAB8hABxQsb)
- [**Python小白也能听懂的入门课**](https://www.aliyundrive.com/s/vYAsTb8ivwb)

#### **Java**
- [**程序设计入门Java语言**](https://www.aliyundrive.com/s/6QmN1WnWP5e)

#### **C语言**
- [**程序设计入门C语言**](https://www.aliyundrive.com/s/si53fTyyZUN)

#### **Go语言**
- [**难得的Go语言实战开发好课**](https://www.aliyundrive.com/s/DPBWYS1wXaR)

### **健康**
> 提取码
~~~
5j5k
~~~
- [**中医生活养生系列**](https://www.aliyundrive.com/s/QWGjwT5W1Dx)
- [**改变你未来九节运动科学课程**](https://www.aliyundrive.com/s/hxrzuVEefbP)
- [**气质跑者的形象修炼课**](https://www.aliyundrive.com/s/1KR6ihTzoa5)

#### **减脂**
- [**不同人群的减脂方案**](https://www.aliyundrive.com/s/gNgCcj6GpLs)
- [**大牌明星都在练的懒人瘦身法**](https://www.aliyundrive.com/s/eDUtQymwYZq)
- [**—瘦就是一辈子，科学不复胖**](https://www.aliyundrive.com/s/dB7SmkfvpoU)
- [**做自己的减脂营养师视频课程**](https://www.aliyundrive.com/s/VrWzFQfKHSK)
- [**科学减肥 10天极速减肥法**](https://www.aliyundrive.com/s/guNHM5qsmgu)
- [**21天养成高效易瘦体质行动营**](https://www.aliyundrive.com/s/2QMoMTRy7vZ)

### **形象**
> 提取码
~~~
um37
~~~
- [**协和医学院皮肤博士13堂美肤课**](https://www.aliyundrive.com/s/iuYxtXa5rTU)
- [**OS男士个人形象**](https://www.aliyundrive.com/s/HqgqCxwjmV2)
- [**男士穿搭指南**](https://www.aliyundrive.com/s/yzXTZrWm8WL)

### **兼职**
> 提取码
~~~
jzzq
~~~
- [**读书变现营**](https://www.aliyundrive.com/s/Dkbne9RzXYz)
- [**头条金牌写手教你读书赚钱**](https://www.aliyundrive.com/s/EBf9uKBZ4Mf)

### **工作**
> 提取码
~~~
gzzq
~~~
- [**学会做出成功让HR心动的个人简历**](https://www.aliyundrive.com/s/KA8NvGZohHE)
- [**老板不会讲的13节职场速胜课**](https://www.aliyundrive.com/s/B3FfYkBcAR7)

#### **办公软件**
- [**Excel大神上分攻略视频课程**](https://www.aliyundrive.com/s/2PMkb7cVW9Z)
- [**21节课带你掌握WPS表格**](https://www.aliyundrive.com/s/cXkahnqd53M)

##### **PPT**
- [**PPT设计急诊室大神之路**](https://www.aliyundrive.com/s/QJAqqwstN9e)
- [**做出优雅PPT的极速训练课**](https://www.aliyundrive.com/s/CV3J45UjsTs)

### **提升**
> 提取码
~~~
8t7s
~~~
- [**考霸训练营**](https://www.aliyundrive.com/s/SqfU9h1oR9A)
- [**颠覆脑力，让你比别人聪明十倍的学习力!**](https://www.aliyundrive.com/s/HjsTUFYesdJ)
- [**18堂时间管理课**](https://www.aliyundrive.com/s/YZ1KFamc979)
- [**人人用得上的思维导图课，有效提升学习力**](https://www.aliyundrive.com/s/WcYtPnp97Dj)
- [**学霸的高效学习法0基础掌握各种技能**](https://www.aliyundrive.com/s/rFwRFM7CFkX)
- [**跳出定式 打开思维的世界**](https://www.aliyundrive.com/s/28YYTc8N3QF)
- [**人生的自我管理必修课程**](https://www.aliyundrive.com/s/MUhbELLB1Wo)
- [**林超：给年轻人的跨学科工具箱**](https://www.aliyundrive.com/s/VnNcAj3s4Ex)
- [**用印象笔记打造第二大脑课程**](https://www.aliyundrive.com/s/SP7nK5Eu3EP)
- [**提高效率人人必备的聪明工作法**](https://www.aliyundrive.com/s/dX9XtSrEYf3)
- [**清华学霸学习法：拒绝死记硬背**](https://www.aliyundrive.com/s/eHDyZzWXSME)

### **营销**
> 提取码
~~~
9yx9
~~~
- [**老路商学课**](https://www.aliyundrive.com/s/iHNq3ZQdYWD)
- [**新商业模式的36个底层逻辑**](https://www.aliyundrive.com/s/ZR65jaF8izz)
- [**商业产品经理的赚钱思路**](https://www.aliyundrive.com/s/rGL4n39sTDs)
- [**拼多多零基础教程**](https://www.aliyundrive.com/s/c3e4TQn3HGb)

#### **销售**
- [**销售技巧：用一半时间拿两倍订单**](https://www.aliyundrive.com/s/FXNFKgyCDU5)
- [**特训7天你也能成为销售高手**](https://www.aliyundrive.com/s/tUbhkgztdrm)

#### **新媒体**
- [**新媒体营销实战方法论**](https://www.aliyundrive.com/s/KThjGKF9FY4)

##### **文案**
- [**朋友圈销售成就计划文案绝杀**](https://www.aliyundrive.com/s/rACrKAHfmUU)
- [**只说话就能火的短视频文案课**](https://www.aliyundrive.com/s/rkDSrCWVvCX)

##### **公众号**
- [**微信公众号排版课**](https://www.aliyundrive.com/s/fP4G7X3L1Q3)
- [**公众号10W+标题常用的18个标题法则**](https://www.aliyundrive.com/s/ZkrESR24ann)
- [**馒头公众号运营**](https://www.aliyundrive.com/s/EBj2LNwjksc)

##### **品牌IP**
- [**杨天真：如何打造个人品牌，为自己代言**](https://www.aliyundrive.com/s/CcHwwB6xdEK)
- [**日入十万的个人品牌课**](https://www.aliyundrive.com/s/RGNHbWzKxTg)
- [**私域IP引流变现视频课程**](https://www.aliyundrive.com/s/9qWSfsQxkST)
- [**抖音商业IP起号核心实操课程**](https://www.aliyundrive.com/s/ogNKmJXcfxo)

##### **短视频**
- [**短视频底层方法论视频课程**](https://www.aliyundrive.com/s/hRq2Zs9Gqin)
- [**从0开始打造爆款抖音号**](https://www.aliyundrive.com/s/4mB5wAtV1Pm)
- [**黑马程序员人人都能学会的短视频运营体系课程**](https://www.aliyundrive.com/s/n14TJvyFM4j)
- [**一站式掌握短视频编导必备技能**](https://www.aliyundrive.com/s/WXcoZFKdp5z)

# 持续更新中...

网站内容会持续保持更新，欢迎收藏品鉴！